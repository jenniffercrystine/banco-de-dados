﻿-- Schema: public

-- DROP SCHEMA public;

--CREATE SCHEMA public
  --AUTHORIZATION postgres;

--GRANT ALL ON SCHEMA public TO postgres;
--GRANT ALL ON SCHEMA public TO public;
--COMMENT ON SCHEMA public
  --IS 'standard public schema';


CREATE TABLE Turma(

id_turma SERIAL NOT NULL PRIMARY KEY,
semestre INT NOT NULL,
codigo_turma CHAR(5) NOT NULL UNIQUE,
serie VARCHAR(20) NOT NULL

);

INSERT INTO Turma (semestre, codigo_turma, serie) VALUES ('201901', '12345', 'setima serie');

INSERT INTO Turma VALUES (default, 201902, '19985', 'nono ano');

UPDATE Turma set codigo_turma = '11101' where id_turma = 2;

-- DELETE FROM Turma WHERE id_turma = 1;


SELECT * FROM Turma;
INSERT INTO Turma VALUES (default, 212365, '58748', 'oitavo ano');
INSERT INTO Turma VALUES (default, 201987, '00071', 'quinto ano');
INSERT INTO Turma VALUES (default, 256634, '55441', 'nono ano');
INSERT INTO Turma VALUES (default, 123658, '14525', 'ensino médio');
INSERT INTO Turma VALUES (default, 221336, '58256', 'quarto ano');
INSERT INTO Turma VALUES (default, 548877, '00355', 'terceiro ano');
INSERT INTO Turma VALUES (default, 258747, '21336', 'segundo ano');

SELECT * FROM Turma WHERE serie = 'nono ano';

SELECT * FROM Turma WHERE serie = 'oitavo ano' ORDER BY codigo_turma;

CREATE TABLE Aluno(
id_aluno SERIAL PRIMARY KEY NOT NULL,
cpf CHAR(11) UNIQUE,
cr DECIMAL,
nome VARCHAR(50) NOT NULL,
matricula CHAR(5) NOT NULL UNIQUE,
fk_id_turma INT REFERENCES Turma(id_turma)

);

INSERT INTO Aluno VALUES (default, '11254002588', 7.1, 'Barbara', '02554', 2);
INSERT INTO Aluno VALUES (default, '21545802588', 4.2, 'Carlos', '26974', 2);
INSERT INTO Aluno VALUES (default, '01424002588', 10, 'Alina', '05854', 21);
INSERT INTO Aluno VALUES (default, '16424026580', 3.6, 'Pine', '09584', 3);
INSERT INTO Aluno VALUES (default, '16498574188', 8.5, 'Tahís', '08564', 16);
INSERT INTO Aluno VALUES (default, '16424265888', 7.0, 'Rana', '02236', 17);
INSERT INTO Aluno VALUES (default, '16588844545', 6.6, 'Pamela', '04587', 20);




CREATE TABLE Professor(
id_professor SERIAL PRIMARY KEY NOT NULL,
nome VARCHAR(50) NOT NULL,
cpf CHAR(11) UNIQUE,
matricula CHAR(5) NOT NULL UNIQUE

);

INSERT INTO Professor VALUES (default, 'Carolina', '1674002588', '08754');

INSERT INTO Professor VALUES (default, 'Diogo', '1674087588', '08005');

INSERT INTO Professor VALUES (default, 'Carlos', '21221698948', '89954');

INSERT INTO Professor VALUES (default, 'João', '1674987488', '00254');

INSERT INTO Professor VALUES (default, 'Lucas', '5987002588', '08862');

SELECT * FROM Professor WHERE serie = 'oitavo ano';



CREATE TABLE Disciplina(
id_disciplina SERIAL PRIMARY KEY NOT NULL,
nome VARCHAR(25) NOT NULL,
ementa VARCHAR(280) NOT NULL

);

INSERT INTO Disciplina VALUES (default, 'Matemática', 'Objetivo: Ensinar os alunos a calcular');
INSERT INTO Disciplina VALUES (default, 'Português', 'Objetivo: Ensinar os alunos a ler');
INSERT INTO Disciplina VALUES (default, 'Geografia', 'Objetivo: Ensinar os alunos a reconhecerem conflitos');
INSERT INTO Disciplina VALUES (default, 'Historia', 'Objetivo: Ensinar os alunos a terem consciência');
INSERT INTO Disciplina VALUES (default, 'Química', 'Objetivo: Ensinar os alunos a saber que elementos químicos são perigosos');


CREATE TABLE Sala(
id_sala SERIAL NOT NULL PRIMARY KEY,
andar INT NOT NULL,
numero INT NOT NULL UNIQUE,
capacidade INT NOT NULL

);
ALTER TABLE Sala
ADD COLUMN complemento VARCHAR(15);

INSERT INTO Sala VALUES (default, 5, 520, 60);
INSERT INTO Sala VALUES (default, 1, 140, 60);
INSERT INTO Sala VALUES (default, 15, 1510, 60);
INSERT INTO Sala VALUES (default, 9, 940, 60);
INSERT INTO Sala VALUES (default, 2, 220, 60);
INSERT INTO Sala VALUES (default, 3, 320, 70, 'LABORATÓRIO');

CREATE TABLE Leciona(
id_leciona SERIAL NOT NULL PRIMARY KEY,
fk_id_disciplina INT REFERENCES Disciplina(id_disciplina),
fk_id_professor INT REFERENCES Professor(id_professor)
);

INSERT INTO Leciona VALUES (default, 1, 2);
INSERT INTO Leciona VALUES (default, 2, 3);
INSERT INTO Leciona VALUES (default, 5, 4);
INSERT INTO Leciona VALUES (default, 3, 1);
INSERT INTO Leciona VALUES (default, 4, 5);


CREATE TABLE Aula(
id_aula SERIAL NOT NULL PRIMARY KEY,
horario TIME NOT NULL,
codigo INT NOT NULL UNIQUE,
fk_id_sala INT REFERENCES Sala(id_sala),
fk_id_leciona INT REFERENCES Leciona(id_leciona)

);

INSERT INTO Aula VALUES (default, '15:00:00', 2565, 1, 2);
INSERT INTO Aula VALUES (default, '10:00:00', 8953, 1, 2);
INSERT INTO Aula VALUES (default, '09:00:00', 1265, 1, 2);
INSERT INTO Aula VALUES (default, '08:00:00', 2258, 1, 2);
INSERT INTO Aula VALUES (default, '07:30:00', 8792, 1, 2);


CREATE TABLE Assiste(
id_assiste SERIAL NOT NULL PRIMARY KEY,
fk_id_aula INT REFERENCES Aula(id_aula),
fk_id_aluno INT REFERENCES Aluno(id_aluno)

);

INSERT INTO Assiste VALUES (default, 1, 16);
INSERT INTO Assiste VALUES (default, 1, 16);
INSERT INTO Assiste VALUES (default, 2, 17);
INSERT INTO Assiste VALUES (default, 3, 18);
INSERT INTO Assiste VALUES (default, 4, 22);
INSERT INTO Assiste VALUES (default, 5, 21);
INSERT INTO Assiste VALUES (default, 4, 20);
