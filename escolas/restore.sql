--
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.11
-- Dumped by pg_dump version 9.6.11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_pkey;
ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_matricula_key;
ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_cpf_key;
ALTER TABLE public.professor ALTER COLUMN id_professor DROP DEFAULT;
DROP SEQUENCE public.professor_id_professor_seq;
DROP TABLE public.professor;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: professor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professor (
    id_professor integer NOT NULL,
    nome character varying(50) NOT NULL,
    cpf character(11),
    matricula character(5) NOT NULL
);


ALTER TABLE public.professor OWNER TO postgres;

--
-- Name: professor_id_professor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professor_id_professor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_id_professor_seq OWNER TO postgres;

--
-- Name: professor_id_professor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professor_id_professor_seq OWNED BY public.professor.id_professor;


--
-- Name: professor id_professor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor ALTER COLUMN id_professor SET DEFAULT nextval('public.professor_id_professor_seq'::regclass);


--
-- Data for Name: professor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professor (id_professor, nome, cpf, matricula) FROM stdin;
\.
COPY public.professor (id_professor, nome, cpf, matricula) FROM '$$PATH$$/2203.dat';

--
-- Name: professor_id_professor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professor_id_professor_seq', 5, true);


--
-- Name: professor professor_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_cpf_key UNIQUE (cpf);


--
-- Name: professor professor_matricula_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_matricula_key UNIQUE (matricula);


--
-- Name: professor professor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (id_professor);


--
-- PostgreSQL database dump complete
--

