﻿-- Schema: public

-- DROP SCHEMA public;

CREATE SCHEMA public
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public
  IS 'standard public schema';

ALTER TABLE Sala
ADD COLUMN complemento  VARCHAR(15) NOT NULL Default 'Sala';


ALTER TABLE Turma
ADD COLUMN qtd_alunos INT NOT NULL Default 30;

ALTER TABLE Aluno
ADD COLUMN endereco VARCHAR(35) NOT NULL Default 'Não informado';




